let collection = [];

// Write the queue functions below.
const print = () => {
	return collection
}



const enqueue = (name) => {
	collection.push(name);
	return collection;
}



const dequeue = () => {
	collection.shift();
	return collection;
}



const front = () => {
	return collection[0]
}



const size = () => {
	return collection.length;
}



const isEmpty = () => {
	if(collection.length >= 0){
		return false
	} else {
		return true
	}
}


/*console.log(print());
console.log(enqueue('John'))
console.log(print());
console.log(enqueue('Jane'));
console.log(print());
console.log(dequeue());
console.log(print());
console.log(enqueue('Bob'));
console.log(print());
console.log(enqueue('Cherry'));
console.log(print());
console.log(front());
console.log(size());
console.log(isEmpty());*/

module.exports = {
print,
enqueue,
dequeue,
front,
size,
isEmpty
};